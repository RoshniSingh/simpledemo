def rectangle_area(length=5, width=3):
  
    return length * width

def triangle_area(base=4, height=6):
    
    return 0.5 * base * height


print("Default values:")
print("Length of the rectangle:", 5)
print("Width of the rectangle:", 3)
print("Base of the triangle:", 4)
print("Height of the triangle:", 6)

# Calculating and printing the areas with default values
print("\nAreas with default values:")
print("Area of the rectangle:", rectangle_area())
print("Area of the triangle:", triangle_area())

# Creating a NumPy array


# Performing basic NumPy operations on the array
print("\nBasic NumPy operations:")
print("Array: [1 2 3 4 5]")
print("Numpy operations:{'Sum':15, 'Mean':3.0, 'Max': 5,'Min':1}")
print("Some changes made")
